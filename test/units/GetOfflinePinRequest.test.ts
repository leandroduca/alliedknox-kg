import {expect} from '../init';
import 'mocha';
import { GetOfflinePinRequest } from '../../src/models/GetOfflinePinRequest';

describe("Test GetOfflinePinRequest", () => {
    
    it("Requisição deve ter os parâmetros obrigatórios (deviceUid e challenge).", () => {
        let data = { deviceUid: "231341234123413", challenge: "231341234123413" };

        expect(() => {
            new GetOfflinePinRequest(data);
        }).not.to.Throw(Error);
    });


    it("Requisição não deve ter um dos parâmetros obrigatórios (challenge).", () => {
        let data = { deviceUid: "231341234123413" };

        expect(() => {
            new GetOfflinePinRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição não deve ter nenhum parâmetro obrigatório.", () => {
        let data = {};

        expect(() => {
            new GetOfflinePinRequest(data);
        }).to.Throw(Error);
    });

});