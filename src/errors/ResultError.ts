import { Error } from './Error';

class ResultError {
    
    public result: string;
    public error: Error;
}

export { ResultError };