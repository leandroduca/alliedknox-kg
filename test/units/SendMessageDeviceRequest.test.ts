import {expect} from '../init';
import 'mocha';
import { SendMessageDeviceRequest } from '../../src/models/SendMessageDeviceRequest';

describe("Test SendMessageDeviceRequest", () => {
    
    it("Requisição deve ter os parâmetros obrigatórios e um identeificador do celular (approveId).", () => {
        let data = { 
            approveId: ["231341234123413"], 
            message: "message"
        };

        expect(() => {
            new SendMessageDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição deve ter os parâmetros obrigatórios e um identeificador do celular (deviceUid).", () => {
        let data = { 
            deviceUid: ["231341234123413"],
            message: "message",
        };

        expect(() => {
            new SendMessageDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição deve ter os parâmetros obrigatórios e um identeificador do celular (objectId).", () => {
        let data = { 
            objectId: ["231341234123413"],
            message: "message",
        };

        expect(() => {
            new SendMessageDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter um dos parâmetros obrigatórios (message).", () => {
        let data = { 
            objectId: ["231341234123413"],
            tel: "1324123412341",
            message: "message",
        };

        expect(() => {
            new SendMessageDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter um dos parâmetros obrigatórios (messageType).", () => {
        let data = { 
            objectId: ["231341234123413"],
            messageType: "message",
        };

        expect(() => {
            new SendMessageDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter parâmetro obrigatório (message ou messageType).", () => {
        let data = {objectId: ["231341234123413"]};

        expect(() => {
            new SendMessageDeviceRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição não deve ter nenhum parâmetro obrigatório.", () => {
        let data = {};

        expect(() => {
            new SendMessageDeviceRequest(data);
        }).to.Throw(Error);
    });
});