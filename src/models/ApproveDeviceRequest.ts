import IValidator from "../interfaces/IValidator";
import { ApproveDevice } from "./ApproveDevice";
import { isArray } from "util";
import { ValidateError } from "../errors/ValidateError";

class ApproveDeviceRequest implements IValidator{
    
    public approveList: ApproveDevice[];

    public constructor(data) {

        this.approveList = data.approveList;

        this.validate();
    }

    public validate = () => {
        
        if (!isArray(this.approveList) || this.approveList.length == 0) 
            throw new ValidateError("approveList is required.");

        this.approveList.forEach(i => i = new ApproveDevice(i));
    }
}

export { ApproveDeviceRequest };