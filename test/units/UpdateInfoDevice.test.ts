import {expect} from '../init';
import 'mocha';
import { UpdateInfoDevice } from '../../src/models/UpdateInfoDevice';

describe("Test UpdateInfoDevice", () => {
    
    it("Requisição deve ter o parâmetro obrigatório (deviceUid).", () => {
        let data = { deviceUid: "123412412341234"};

        expect(() => {
            new UpdateInfoDevice(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter o parâmetro obrigatório (deviceUid).", () => {
        let data = { };

        expect(() => {
            new UpdateInfoDevice(data);
        }).to.Throw(Error);
    });

    it("Requisição com parâmetro inválido (paidCount).", () => {
        let data = { deviceUid: "123412412341234", paidCount: 0 };

        expect(() => {
            new UpdateInfoDevice(data);
        }).to.Throw(Error);
    });

    it("Requisição com parâmetro inválido (totalCount).", () => {
        let data = { deviceUid: "123412412341234", totalCount: 0 };

        expect(() => {
            new UpdateInfoDevice(data);
        }).to.Throw(Error);
    });

});