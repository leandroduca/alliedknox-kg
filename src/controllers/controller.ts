import { Request, Response } from 'express';
import { Service } from '../services/service';

export const checkAuthorization = async (req: Request, res: Response) => {

    const service = new Service();

    const response = await service.checkAuthorization();
    console.log(response);

    res.status(200).json(response);
}

export const getOfflinePinDevice = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.getOfflinePinDevice(data);
    console.log(response);

    res.status(200).json(response);
}

export const getPinDevice = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.getPinDevice(data);
    console.log(response);

    res.status(200).json(response);
}

export const approveDevices = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.approveDevices(data);
    console.log(response);

    res.status(200).json(response);
}

export const blinkDevice = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.blinkDevices(data);
    console.log(response);

    res.status(200).json(response);
}

export const getDevice = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.getDevices(data);
    console.log(response);

    res.status(200).json(response);
}

export const lockDevices = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.lockDevices(data);
    console.log(response);

    res.status(200).json(response);
}

export const deleteDevices = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.deleteDevices(data);
    console.log(response);

    res.status(200).json(response);
}

export const unlockDevices = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.unlockDevices(data);
    console.log(response);

    res.status(200).json(response);
}

export const addLicense = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.addLicense(data);
    console.log(response);

    res.status(200).json(response);
}

export const getLicenses = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.getLicenses(data);
    console.log(response);

    res.status(200).json(response);
}

export const sendMessageDevices = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.sendMessageDevices(data);
    console.log(response);

    res.status(200).json(response);
}

export const updateInfoDevices = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.updateInfoDevices(data);
    console.log(response);

    res.status(200).json(response);
}

export const completeDevices = async (req: Request, res: Response) => {

    const service = new Service();

    let data = req.body;

    const response = await service.completeDevices(data);
    console.log(response);

    res.status(200).json(response);
}