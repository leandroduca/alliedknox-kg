import IValidator from "../interfaces/IValidator";
import { ValidateError } from "../errors/ValidateError";
import { isArray } from "util";


class GetDevice implements IValidator {

    public filter: any;
    public pageNum: number; 
    public pageSize: number;
    public search: string;

    public constructor (data) {
        
        this.filter = data.filter;
        this.pageNum = data.pageNum;
        this.pageSize = data.pageSize;
        this.search = data.search;

        this.validate();
    }

    public validate = () => {

        if (this.pageNum > 0) {}
    }
}

export { GetDevice };