import { Client } from '../clients/client';
import { ApproveDeviceRequest } from '../models/ApproveDeviceRequest';
import { BlinkDeviceRequest } from "../models/BlinkDeviceRequest";
import { LockDeviceRequest } from '../models/LockDeviceRequest';
import { DeleteDeviceRequest } from '../models/DeleteDeviceRequest';
import { UnlockDeviceRequest } from '../models/UnlockDeviceRequest';
import { AddLicenseRequest } from '../models/AddLicenseRequest';
import { GetLicensesRequest } from '../models/GetLicensesRequest';
import { SendMessageDeviceRequest } from '../models/SendMessageDeviceRequest';
import { UpdateInfoDeviceRequest } from '../models/UpdateInfoDeviceRequest';
import { GetDevice } from '../models/GetDevice';
import { GetPinRequest } from '../models/GetPinRequest';
import { GetOfflinePinRequest } from '../models/GetOfflinePinRequest';
import { CompleteDeviceRequest } from '../models/CompleteDeviceRequest';
import { CheckContaKgService } from '../utils/checkContaKgService';

class Service {

    private client: Client;
    private checkContaKgService: CheckContaKgService;

    public constructor() {

        this.client = new Client();
        this.checkContaKgService = new CheckContaKgService();    
    }

    public async checkAuthorization() {

        let res = await this.client.checkAuthorization();
        return res;
    }

    public async verifyContaKg(deviceUid: string) {        
        const result = await this.checkContaKgService.verifyKg(deviceUid);
        //console.log('RESULT: ', result);
        let contaKg = result[0].conta_kg;
        //console.log('CONTAKG: ', contaKg);
        if (contaKg != null) {
            return true;
        }
        return false;
    }


    public async getOfflinePinDevice(data: any) {
        let getOfflinePinRequest = new GetOfflinePinRequest(data);

        const isContaAntiga = await this.verifyContaKg(getOfflinePinRequest.deviceUid);

        let res = await this.client.getOfflinePinDevice(getOfflinePinRequest, isContaAntiga);
        return res;
    }

    public async getPinDevice(data: any) {
        let getPinRequest = new GetPinRequest(data);

        const isContaAntiga = await this.verifyContaKg(getPinRequest.deviceUid);

        let res = await this.client.getPinDevice(getPinRequest, isContaAntiga);
        return res;
    }

    public async approveDevices(data: any) {
        let approveDeviceRequest = new ApproveDeviceRequest(data);

        let res = await this.client.approveDevices(approveDeviceRequest);
        return res;
    }

    public async blinkDevices(data: any) {
        let blinkDeviceRequest = new BlinkDeviceRequest(data);

        const isContaAntiga = await this.verifyContaKg(blinkDeviceRequest.deviceUid[0]);

        let res = await this.client.blinkDevices(blinkDeviceRequest, isContaAntiga);
        return res;
    }

    public async getDevices(data: any) {
        let getDevices = new GetDevice(data);
        
        if (getDevices.search) {
            const isContaAntiga = await this.verifyContaKg(getDevices.search);

            let res = await this.client.getDevices(getDevices, isContaAntiga);
            return res;
        }
        
        const isContaAntiga = false;

        let res = await this.client.getDevices(getDevices, isContaAntiga);
        return res;
    }

    public async lockDevices(data: any) {
        let lockDeviceRequest = new LockDeviceRequest(data);

        const isContaAntiga = await this.verifyContaKg(lockDeviceRequest.deviceUid[0]);

        let res = await this.client.lockDevices(lockDeviceRequest, isContaAntiga);
        return res;
    }

    public async deleteDevices(data: any) {
        let deleteDeviceRequest = new DeleteDeviceRequest(data);

        let res = await this.client.deleteDevices(deleteDeviceRequest);
        return res;
    }

    public async unlockDevices(data: any) {
        let unlockDeviceRequest = new UnlockDeviceRequest(data);
                
        const isContaAntiga = await this.verifyContaKg(unlockDeviceRequest.deviceUid[0]);
        //console.log('isContaAntiga: ', isContaAntiga);
        let res = await this.client.unlockDevices(unlockDeviceRequest, isContaAntiga);
        return res;
    }

    public async addLicense(data: any) {
        let addLicenseRequest = new AddLicenseRequest(data);

        let res = await this.client.addLicense(addLicenseRequest);
        return res;
    }

    public async getLicenses(data: any) {
        let getLicensesRequest = new GetLicensesRequest(data);

        let res = await this.client.getLicenses(getLicensesRequest);
        return res;
    }

    public async sendMessageDevices(data) {
        let sendMessageDeviceRequest = new SendMessageDeviceRequest(data);

        const isContaAntiga = await this.verifyContaKg(sendMessageDeviceRequest.deviceUid[0]);

        let res = await this.client.sendMessageDevices(sendMessageDeviceRequest, isContaAntiga);
        return res;
    }

    public async updateInfoDevices(data: any) {
        let updateInfoDeviceRequest = new UpdateInfoDeviceRequest(data);

        let res = await this.client.updateInfoDevices(updateInfoDeviceRequest);
        return res;
    }

    public async completeDevices(data: any) {
        let completeDeviceRequest = new CompleteDeviceRequest(data);

        let res = await this.client.completeDevices(completeDeviceRequest);
        return res;
    }
}

export { Service }