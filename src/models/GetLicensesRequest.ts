import IValidator from "../interfaces/IValidator";
import { ValidateError } from "../errors/ValidateError";
import { isNullOrUndefined } from "util";

class GetLicensesRequest implements IValidator{
    
    public filter: {
        "status": string[];
    };
    public search: string;
    public pageNum: number;
    public pageSize: number;

    public constructor (data){
        this.filter = data.filter;
        this.search = data.search;
        this.pageNum = data.pageNum;
        this.pageSize = data.pageSize;

        this.validate();
    }

    public validate = () => {

        if (isNullOrUndefined(this.pageNum))
            throw new ValidateError("pageNum is required.");

        if (isNullOrUndefined(this.pageSize))
            throw new ValidateError("pageSize is required.");            
    }
}

export { GetLicensesRequest };