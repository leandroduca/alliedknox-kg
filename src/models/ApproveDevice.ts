import IValidator from "../interfaces/IValidator";
import { ValidateError } from "../errors/ValidateError";
import { isNullOrUndefined } from "util";

class ApproveDevice implements IValidator {

    public deviceUid: string;
    public approveId: string;
    public approveComment: string;

    public constructor (data) {

        this.deviceUid = data.deviceUid;
        this.approveId = data.approveId;
        this.approveComment = data.approveComment;

        this.validate();
    }

    public validate  = () => {

        if (!this.deviceUid)
            throw new ValidateError("deviceUid is required.");
    }
}

export { ApproveDevice }