import { Err } from 'allied-kernel';

class KnoxError extends Err {

    public constructor(message: string, statusCode: number) {

        super(message, statusCode, 'knox-error');
    }
} 

export { KnoxError };