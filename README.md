# Serverless Template

Esse é um projeto de template que pode ser utilizado para outros projetos que usam a arquitetura serverless e modulos proprietários.

# Começando...

O primeiro passo é ter o pacote serverless instalado globalmente. Caso já possua, pode pular esse próximo passo. 
- yarn global add serverless 

Ao fazer o clone do projeto, na pasta do mesmo, rodar 'yarn install' para baixar as dependencias. Após, basta iniciar o serverless com 'yarn start'. Isso deve liberar acesso a um servidor e 3 rotas.

- Scripts Iniciais
    - yarn install
    - yarn start

- Rotas
    - http://localhost:3001/  -> GET
    - http://localhost:3001/  -> POST
    - http://localhost:3001/  -> PUT

## Desenvolvimento

O projeto usa (e os seus pares também devem) typescrispt, a metodologia TDD e a arquitetura serverless em ambiente AWS.
Abaixo, seguem as ferramentas e scripts usados no processo.

### Ferramentas
- Hooks:
    - [Husky](https://github.com/typicode/husky) (git hooks)
- Padrão de código
    - [Eslint](https://eslint.org/) (lint padrão)
    - [TypeScript ESLint](https://github.com/typescript-eslint/typescript-eslint)(lint para typescript)

- Serverless
    - [Serverless](https://serverless.com/framework/docs/providers/aws/guide/) (ambiente sem servidor)
        - Local
            - CustomOffline (compilar e observar arquivos typescript)
            - [Serverless Offline](https://serverless.com/plugins/serverless-offline/) (rodar ambiente serverless local)
        - AWS
            - CustomDeployTypescript (compilar e empacotar typescript para aws)
            - [Serverless Tracing](https://serverless.com/plugins/serverless-plugin-tracing/) (Habilitar rastreio x-Ray)
- Testes:
    - [Mocha](https://mochajs.org/) (rodar testes)
    - [Chai](https://www.chaijs.com/) (assertividade)
    - [Sinon](https://sinonjs.org/) (mock de funções, spies, etc)
    - [Nyc](https://github.com/istanbuljs/nyc) (cobertura de testes)

## Yarn Scripts 
    - yarn deploy: Executa o serverless deploy. Faz o empacotamente e implanta na conta aws.

    - yarn start: executa o serverless com o plugin offline programar sem servidor localmente.

    - yarn lint: executa o analisador de padrões e retorna os erros/avisos que forem encontrados.

    - yarn test: executa o nyc com o mocha, exibindo os testes e a cobertura dos mesmos.

## Yarn Hooks
    - git push: Ao tentar fazer o push, serão executados os scripts de lint e de test.
      SOMENTE se os dois não tiverem problemas o push ocorre de fato. Caso algum teste não passe ou tenha algum ERRO de lint o processo é interrompido.
    