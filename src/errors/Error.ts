class Error {

    public code: number;
    public message: string;
    public reason: string;
}

export { Error };