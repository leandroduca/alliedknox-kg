import {expect} from '../init';
import 'mocha';
import { UnlockDeviceRequest } from '../../src/models/UnlockDeviceRequest';

describe("Test UnlockDeviceRequest", () => {
    
    it("Requisição deve ter um os parâmetros obrigatórios (approveId).", () => {
        let data = { approveId: ["231341234123413"] };

        expect(() => {
            new UnlockDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição deve ter um os parâmetros obrigatórios (deviceUid).", () => {
        let data = { deviceUid: ["231341234123413"] };

        expect(() => {
            new UnlockDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição deve ter um os parâmetros obrigatórios (objectId).", () => {
        let data = { objectId: ["231341234123413"] };

        expect(() => {
            new UnlockDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter nenhum parâmetro obrigatório.", () => {
        let data = {};

        expect(() => {
            new UnlockDeviceRequest(data);
        }).to.Throw(Error);
    });
});