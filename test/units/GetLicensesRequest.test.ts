import {expect} from '../init';
import 'mocha';
import { GetLicensesRequest } from '../../src/models/GetLicensesRequest';

describe("Test GetLicensesRequest", () => {
    
    it("Requisição deve ter os parâmetros obrigatórios (pageNum, pageSize).", () => {
        let data = { pageNum: 0, pageSize: 10 };

        expect(() => {
            new GetLicensesRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter o parâmetro obrigatório (pageNum).", () => {
        let data = { pageSize: 10 };

        expect(() => {
            new GetLicensesRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição não deve ter o parâmetro obrigatório (pageSize).", () => {
        let data = { pageNum: 0 };

        expect(() => {
            new GetLicensesRequest(data);
        }).to.Throw(Error);
    });
});