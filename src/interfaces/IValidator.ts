interface IValidator {
    
    validate();
}

export default IValidator;