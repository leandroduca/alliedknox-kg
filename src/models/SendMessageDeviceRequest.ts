import IValidator from "../interfaces/IValidator";
import { ValidateError } from "../errors/ValidateError";
import { isArray, isNullOrUndefined } from "util";

class SendMessageDeviceRequest implements IValidator {
    
    public approveId: string[];
    public deviceUid: string[]; 
    public objectId: string[];
    public email: string;
    public tel: string;
    public message: string;
    public messageType: number;

    public constructor (data) {

        this.approveId = data.approveId;
        this.deviceUid = data.deviceUid;
        this.objectId = data.objectId;
        this.email = data.email;
        this.tel = data.tel;
        this.message = data.message;
        this.messageType = data.messageType;

        this.validate();
    }

    public validate = () => {

        if (isArray(this.approveId) && this.approveId.length > 0) {}
        else if (isArray(this.deviceUid) && this.deviceUid.length > 0) {}
        else if (isArray(this.objectId) && this.objectId.length > 0) {}
        else throw new ValidateError("At least one parameter, approveId, deviceUid, or objectId, is mandatory.");

        if (!isNullOrUndefined(this.message)) {}
        else if (!isNullOrUndefined(this.messageType)) {}
        else throw new ValidateError("At least one parameter, message or messageType, is mandatory.");
    }
}

export { SendMessageDeviceRequest };