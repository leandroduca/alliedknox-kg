import { HttpClient } from 'allied-kernel';
import { KnexSingleton } from '../utils/KnexSingleton';
import { KnoxError } from '../errors/KnoxError';

class Client {

    private httpClient: HttpClient;

    public constructor() {

        this.httpClient = new HttpClient({
            
            baseURL: process.env.BASE_URL_CLIENT,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Cache-control': 'no-cache'
            }
        });
    }

    private async getKnoxApiToken() { 
        let kn = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || "3306"), process.env.DBPASSWORD, process.env.DBNAME).conn;

        let token = await kn.select('knoxPublicToken.public_token_kg')
            .from('knoxPublicToken')
            .orderBy('id', 'desc')
            .first();

        return { 
            headers: { 
                'x-knox-apitoken': token.public_token_kg,
            } 
        }
    }

    private async getKnoxApiTokenOld() { 
        let kn = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || "3306"), process.env.DBPASSWORD, process.env.DBNAME).conn;

        let token = await kn.select('knoxPublicTokenOld.public_token_kg')
            .from('knoxPublicTokenOld')
            .orderBy('id', 'desc')
            .first();

        return { 
            headers: { 
                'x-knox-apitoken': token.public_token_kg,
            } 
        }
    }

    public async checkAuthorization(): Promise<any> {
        const response = await this.httpClient.get(
            '/authorization',
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async getOfflinePinDevice(data: any, isContaAntiga: any): Promise<any> {
        if (isContaAntiga == true) {
            const response = await this.httpClient.post(
                '/devices/offlineDeviceLockPin',
                data,
                await this.getKnoxApiTokenOld()
            ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });
    
            return response.data;    
        }

        const response = await this.httpClient.post(
            '/devices/offlineDeviceLockPin',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }


    public async getPinDevice(data: any, isContaAntiga: any): Promise<any> {
        if (isContaAntiga == true) {
            const response = await this.httpClient.post(
                '/devices/getPin',
                data,
                await this.getKnoxApiTokenOld()
            ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });
    
            return response.data;
        }

        const response = await this.httpClient.post(
            '/devices/getPin',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async approveDevices(data: any): Promise<any> {
        
        const response = await this.httpClient.post(
            '/devices/approveAsync',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async blinkDevices(data: any, isContaAntiga: any): Promise<any> {
        if (isContaAntiga == true) {
            const response = await this.httpClient.post(
                '/devices/blinkAsync',
                data,
                await this.getKnoxApiTokenOld()
            ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });
    
            return response.data;
        }

        const response = await this.httpClient.post(
            '/devices/blinkAsync',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async getDevices(data: any, isContaAntiga: any): Promise<any> {
        if (isContaAntiga == true) {
            const response = await this.httpClient.post(
                '/devices/list',
                data,
                await this.getKnoxApiTokenOld()
            ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });
    
            return response.data;
        }

        const response = await this.httpClient.post(
            '/devices/list',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async lockDevices(data: any, isContaAntiga: any): Promise<any> {
        if (isContaAntiga == true) {
            const response = await this.httpClient.post(
                '/devices/lockAsync',
                data,
                await this.getKnoxApiTokenOld()
            ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });
    
            return response.data;
        }

        const response = await this.httpClient.post(
            '/devices/lockAsync',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async deleteDevices(data: any): Promise<any> {
        
        const response = await this.httpClient.post(
            '/devices/deleteAsync',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async unlockDevices(data: any, isContaAntiga: any): Promise<any> {
        if (isContaAntiga == true) {
            const response = await this.httpClient.post(
                '/devices/unlockAsync',
                data,
                await this.getKnoxApiTokenOld()
            ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });
    
            return response.data;
        }

        const response = await this.httpClient.post(
            '/devices/unlockAsync',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async addLicense(data: any): Promise<any> {
        
        const response = await this.httpClient.post(
            '/license/add',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async getLicenses(data: any): Promise<any> {
        
        const response = await this.httpClient.post(
            '/license/list',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async sendMessageDevices(data: any, isContaAntiga: any): Promise<any> {
        if (isContaAntiga == true) {
            const response = await this.httpClient.post(
                '/devices/sendMessageAsync',
                data,
                await this.getKnoxApiTokenOld()
            ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });
    
            return response.data;
        }

        const response = await this.httpClient.post(
            '/devices/sendMessageAsync',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async updateInfoDevices(data: any): Promise<any> {
        
        const response = await this.httpClient.post(
            '/devices/updateInfoAsync',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }

    public async completeDevices(data: any): Promise<any> {
        
        const response = await this.httpClient.post(
            '/devices/completeAsync',
            data,
            await this.getKnoxApiToken()
        ).catch((err) => { throw new KnoxError(err.response.data.error.reason, err.response.status) });

        return response.data;
    }
}

export { Client }