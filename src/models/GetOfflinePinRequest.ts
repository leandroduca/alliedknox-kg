import IValidator from "../interfaces/IValidator";
import { ValidateError } from "../errors/ValidateError";
import { isNullOrUndefined } from "util";

class GetOfflinePinRequest implements IValidator {
    
    public approveId: string;
    public deviceUid: string; 
    public objectId: string;
    public challenge: string;

    public constructor (data) {

        this.approveId = data.approveId;
        this.deviceUid = data.deviceUid;
        this.objectId = data.objectId;
        this.challenge = data.challenge;

        this.validate();
    }

    public validate = () => {

        // if (isNullOrUndefined(this.approveId) || this.approveId.trim() == '') {}
        // else if (isNullOrUndefined(this.deviceUid) || this.deviceUid.trim() == '') {}
        // else if (isNullOrUndefined(this.objectId) || this.objectId.trim() == '') {}
        // else throw new ValidateError("At least one parameter, approveId, deviceUid, or objectId, is mandatory.");

        if (!this.deviceUid)
            throw new ValidateError("deviceUid is required.");

        if (!this.challenge)
            throw new ValidateError("challenge is required.");
    }
}

export { GetOfflinePinRequest };