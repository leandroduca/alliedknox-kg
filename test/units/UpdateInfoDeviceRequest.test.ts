import {expect} from '../init';
import 'mocha';
import { UpdateInfoDeviceRequest } from '../../src/models/UpdateInfoDeviceRequest';

describe("Test UpdateInfoDeviceRequest", () => {
    
    it("Requisição deve ter o parâmetro obrigatório não vazio (updateList).", () => {
        let data = { updateList: [{ deviceUid: "231341234123413" }] };

        expect(() => {
            new UpdateInfoDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter o parâmetro obrigatório (updateList).", () => {
        let data = {};

        expect(() => {
            new UpdateInfoDeviceRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição deve ter o parâmetro obrigatório vazio (updateList).", () => {
        let data = { updateList: [{}] };

        expect(() => {
            new UpdateInfoDeviceRequest(data);
        }).to.Throw(Error);
    });
});