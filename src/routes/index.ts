import { Router } from 'express';
import * as controller from '../controllers/controller';
import { asyncMiddleware } from 'allied-kernel';
import { swaggerSetup, swaggerUi } from '../configs/swagger';
import { verifyJWT } from '../utils/jwtValidation';
import * as cors from 'cors';

const router = Router();

router.options('*', cors());

router.use('/', swaggerUi.serve);
router.get('/api-docs', swaggerSetup);

// KG Endpoints 
router.use(['/basic', '/devices', '/device', '/license'], verifyJWT);
router.get('/basic/authorization', asyncMiddleware(controller.checkAuthorization));
router.post('/device/getOfflinePin', asyncMiddleware(controller.getOfflinePinDevice));
router.post('/device/getPin', asyncMiddleware(controller.getPinDevice));
router.post('/license/add', asyncMiddleware(controller.addLicense));
router.post('/license/list', asyncMiddleware(controller.getLicenses));
router.post('/devices/approve', asyncMiddleware(controller.approveDevices));
router.post('/devices/blink', asyncMiddleware(controller.blinkDevice));
router.post('/devices/list', asyncMiddleware(controller.getDevice));
router.post('/devices/lock', asyncMiddleware(controller.lockDevices));
router.post('/devices/delete', asyncMiddleware(controller.deleteDevices));
router.post('/devices/sendMessage', asyncMiddleware(controller.sendMessageDevices));
router.post('/devices/unlock', asyncMiddleware(controller.unlockDevices));
router.post('/devices/updateInfo', asyncMiddleware(controller.updateInfoDevices));
router.post('/devices/complete', asyncMiddleware(controller.completeDevices));

export default router;