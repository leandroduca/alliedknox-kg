import IValidator from "../interfaces/IValidator";
import { ValidateError } from "../errors/ValidateError";
import { isNullOrUndefined } from "util";

class AddLicenseRequest implements IValidator{
    
    public licenseName: string;
    public licenseKey: string;

    public constructor (data){
        this.licenseName = data.licenseName;
        this.licenseKey = data.licenseKey;

        this.validate();
    }

    public validate = () => {

        if (isNullOrUndefined(this.licenseName))
            throw new ValidateError("licenseName is required.");
        if (isNullOrUndefined(this.licenseKey))
            throw new ValidateError("licenseKey is required.");
    }
}

export { AddLicenseRequest };