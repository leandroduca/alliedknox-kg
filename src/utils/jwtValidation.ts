import { AuthError } from "../errors/auth-error";

var jwt = require('jsonwebtoken');

export function verifyJWT(req, res, next){

    var token = req.headers['x-access-token'];
    
    if (!token) throw new AuthError('Login necessário.', 400);
    
    jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
        if (err) throw new AuthError('Falha ao autenticar.', 401);
      
        // if everything it's ok, save on request for next use
        req.userId = decoded.id;
        next();
    });
}