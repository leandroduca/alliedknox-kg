import { UpdateInfoDevice } from "./UpdateInfoDevice";
import { isArray } from "util";
import { ValidateError } from "../errors/ValidateError";
import IValidator from "../interfaces/IValidator";

class UpdateInfoDeviceRequest implements IValidator {

    public updateList: UpdateInfoDevice[];

    public constructor(data) {
        
        this.updateList = data.updateList;

        this.validate();
    }

    public validate = () => {
        
        if (!isArray(this.updateList) || this.updateList.length == 0) 
            throw new ValidateError("updateList is required.");
        
        this.updateList.forEach(i => i = new UpdateInfoDevice(i))
    }
}

export { UpdateInfoDeviceRequest };