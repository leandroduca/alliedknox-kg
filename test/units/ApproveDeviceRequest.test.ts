import {expect} from '../init';
import 'mocha';
import { ApproveDeviceRequest } from '../../src/models/ApproveDeviceRequest';

describe("Test ApproveDeviceRequest", () => {
    
    it("Requisição deve ter o parâmetro obrigatório não vazio (approveList).", () => {
        let data = { approveList: [{ deviceUid: "231341234123413" }] };

        expect(() => {
            new ApproveDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter o parâmetro obrigatório (approveList).", () => {
        let data = {};

        expect(() => {
            new ApproveDeviceRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição deve ter o parâmetro obrigatório vazio (approveList).", () => {
        let data = { approveList: [{}] };

        expect(() => {
            new ApproveDeviceRequest(data);
        }).to.Throw(Error);
    });
});