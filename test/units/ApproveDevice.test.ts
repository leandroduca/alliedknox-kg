import {expect} from '../init';
import 'mocha';
import { ApproveDevice } from '../../src/models/ApproveDevice';

describe("Test ApproveDevice", () => {
    
    it("Requisição deve ter o parâmetro obrigatório (deviceUid).", () => {
        let data = { deviceUid: "123412412341234"};

        expect(() => {
            new ApproveDevice(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter o parâmetro obrigatório (deviceUid).", () => {
        let data = { };

        expect(() => {
            new ApproveDevice(data);
        }).to.Throw(Error);
    });
});