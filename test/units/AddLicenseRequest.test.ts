import {expect} from '../init';
import 'mocha';
import { AddLicenseRequest } from '../../src/models/AddLicenseRequest';

describe("Test AddLicenseRequest", () => {
    
    it("Requisição deve ter os parâmetros obrigatórios (licenseName, licenseKey).", () => {
        let data = { licenseName: "name", licenseKey: "chave" };

        expect(() => {
            new AddLicenseRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter nenhum parâmetro obrigatório.", () => {
        let data = {};

        expect(() => {
            new AddLicenseRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição não deve ter o parâmetro obrigatório (licenseName).", () => {
        let data = { licenseKey: "chave" };

        expect(() => {
            new AddLicenseRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição não deve ter o parâmetro obrigatório (licenseKey).", () => {
        let data = { licenseName: "nome" };

        expect(() => {
            new AddLicenseRequest(data);
        }).to.Throw(Error);
    });
});