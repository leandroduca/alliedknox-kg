import {expect} from '../init';
import 'mocha';
import { BlinkDeviceRequest } from '../../src/models/BlinkDeviceRequest';

describe("Test BlinkDeviceRequest", () => {
    
    it("Requisição deve ter os parâmetros obrigatórios e um identeificador do celular (approveId).", () => {
        let data = { 
            approveId: ["231341234123413"], 
            email: "email@email.com",
            tel: "1324123412341",
            interval: 86400,
            message: "message",
        };

        expect(() => {
            new BlinkDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição deve ter os parâmetros obrigatórios e um identeificador do celular (deviceUid).", () => {
        let data = { 
            deviceUid: ["231341234123413"], 
            email: "email@email.com",
            tel: "1324123412341",
            interval: 86400,
            message: "message",
        };

        expect(() => {
            new BlinkDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição deve ter os parâmetros obrigatórios e um identeificador do celular (objectId).", () => {
        let data = { 
            objectId: ["231341234123413"], 
            email: "email@email.com",
            tel: "1324123412341",
            interval: 86400,
            message: "message",
        };

        expect(() => {
            new BlinkDeviceRequest(data);
        }).not.to.Throw(Error);
    });

    it("Requisição não deve ter um dos parâmetros obrigatórios (email).", () => {
        let data = { 
            objectId: ["231341234123413"],
            tel: "1324123412341",
            interval: 86400,
            message: "message",
        };

        expect(() => {
            new BlinkDeviceRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição não deve ter um dos parâmetros obrigatórios (tel).", () => {
        let data = { 
            objectId: ["231341234123413"], 
            email: "email@email.com",
            interval: 86400,
            message: "message",
        };

        expect(() => {
            new BlinkDeviceRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição não deve ter um dos parâmetros obrigatórios (interval).", () => {
        let data = { 
            objectId: ["231341234123413"], 
            email: "email@email.com",
            tel: "1324123412341",
            message: "message",
        };

        expect(() => {
            new BlinkDeviceRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição não deve ter um dos parâmetros obrigatórios (message).", () => {
        let data = { 
            objectId: ["231341234123413"], 
            email: "email@email.com",
            tel: "1324123412341",
            interval: 86400,
        };

        expect(() => {
            new BlinkDeviceRequest(data);
        }).to.Throw(Error);
    });

    it("Requisição não deve ter nenhum parâmetro obrigatório.", () => {
        let data = {};

        expect(() => {
            new BlinkDeviceRequest(data);
        }).to.Throw(Error);
    });
});