import IValidator from "../interfaces/IValidator";
import { isNullOrUndefined } from "util";
import { ValidateError } from "../errors/ValidateError";

class UpdateInfoDevice implements IValidator {
    
    public approveComment: string;
    public approveId: string;
    public arrearsDays: number;
    public deviceUid: string;
    public paidCount: number;
    public totalCount: number;

    public constructor(data) {

        this.approveComment = data.approveComment;
        this.approveId = data.approveId;
        this.arrearsDays = data.arrearsDays;
        this.deviceUid = data.deviceUid;
        this.paidCount = data.paidCount;
        this.totalCount = data.totalCount;

        this.validate();
    }

    public validate = () => {

        if (!this.deviceUid)
            throw new ValidateError("deviceUid is required.");

        if (this.paidCount <= 0 || this.paidCount > this.totalCount)
            throw new ValidateError("paidCount must be greater than 0 and less than or equal to totalCount");
        
        if (this.totalCount <= 0 || this.totalCount < this.paidCount)
            throw new ValidateError("totalCount must be greater than 0 and greater than or equal to paidCount");
    }
}

export { UpdateInfoDevice };