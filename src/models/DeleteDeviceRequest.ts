import IValidator from "../interfaces/IValidator";
import { ValidateError } from "../errors/ValidateError";
import { isArray } from "util";


class DeleteDeviceRequest implements IValidator {

    public approveId: string[];
    public deviceUid: string[]; 
    public objectId: string[];

    public constructor (data) {
        
        this.approveId = data.approveId;
        this.deviceUid = data.deviceUid;
        this.objectId = data.objectId;

        this.validate();
    }

    public validate = () => {

        if (isArray(this.approveId) && this.approveId.length > 0) {}
        else if (isArray(this.deviceUid) && this.deviceUid.length > 0) {}
        else if (isArray(this.objectId) && this.objectId.length > 0) {}
        else throw new ValidateError("At least one parameter, approveId, deviceUid, or objectId, is mandatory.");
    }
}

export { DeleteDeviceRequest };