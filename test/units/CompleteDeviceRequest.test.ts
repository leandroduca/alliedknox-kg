import {expect} from '../init';
import 'mocha';
import { CompleteDeviceRequest } from '../../src/models/CompleteDeviceRequest';

describe("Test CompleteDeviceRequest", () => {

    it("Requisição deve ter um os parâmetros obrigatórios (deviceUid).", () => {
        let data = { deviceUid: ["231341234123413"] };

        expect(() => {
            new CompleteDeviceRequest(data);
        }).not.to.Throw(Error);
    });


    it("Requisição não deve ter nenhum parâmetro obrigatório.", () => {
        let data = {};

        expect(() => {
            new CompleteDeviceRequest(data);
        }).to.Throw(Error);
    });
});