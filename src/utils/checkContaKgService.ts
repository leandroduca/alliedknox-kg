import { KnexSingleton } from './KnexSingleton'

class CheckContaKgService {
    private connection;
    constructor () {
        this.connection = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || '3306'), process.env.DBPASSWORD, process.env.DBNAME).conn;
    }

    public verifyKg = async (deviceUid: string) => {
        return await this.connection.select('conta_kg')
            .from('linxMovimentoSerial')
            .where('serial', deviceUid);            
    }
}

export { CheckContaKgService }