import IValidator from "../interfaces/IValidator";
import { ValidateError } from "../errors/ValidateError";
import { isArray, isNullOrUndefined } from "util";

class LockDeviceRequest implements IValidator {

    public approveId: string[];
    public deviceUid: string[]; 
    public objectId: string[];
    public email: string;
    public tel: string;
    public message: string;

    public constructor (data) {

        this.approveId = data.approveId;
        this.deviceUid = data.deviceUid;
        this.objectId = data.objectId;
        this.email = data.email;
        this.tel = data.tel;
        this.message = data.message;

        this.validate();
    }

    public validate = () => {

        if (isArray(this.approveId) && this.approveId.length > 0) {}
        else if (isArray(this.deviceUid) && this.deviceUid.length > 0) {}
        else if (isArray(this.objectId) && this.objectId.length > 0) {}
        else throw new ValidateError("At least one parameter, approveId, devieUid, or objectId, is mandatory.");

        if (isNullOrUndefined(this.email))
            throw new ValidateError("email is required.");

        if (isNullOrUndefined(this.tel))
            throw new ValidateError("tel is required.");

        if (isNullOrUndefined(this.message)) 
            throw new ValidateError("message is required.");
    }
}

export { LockDeviceRequest };